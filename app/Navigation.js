import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import HomeScreen from './Screens/HomeScreen';
import ListScreen from './Screens/ListScreen';

const PrimaryNav = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    List: { screen: ListScreen }
  },
  {
    // Default config for all screens
    headerMode: 'none',
    initialRouteName: 'Home',
    navigationOptions: {},
    defaultNavigationOptions: {
      gesturesEnabled: false
    }
  },
);

export default createAppContainer(PrimaryNav);
