import React from 'react';
import AppNavigation from './Navigation';

export default class RootContainer extends React.Component {
  render() {
    return <AppNavigation />;
  }
}
