import { StyleSheet,Dimensions } from 'react-native';
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FAEBD7'
  },
  rowBg: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  listImg: {
    width: height * 0.07,
    height: height * 0.07
  },
  cocktailNameText: {
    color: '#FF9912',
    fontSize: 20,
    marginLeft: width * 0.05,
    width: width * 0.5
  },
  editDeleteIconBg: {
    flexDirection: 'row'
  }
  
});
