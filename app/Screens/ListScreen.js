import React from 'react';
import { SafeAreaView,FlatList,TextInput } from 'react-native';
import { Container,Header, View, Text, Button } from 'native-base';
import { connect } from 'react-redux';
import {
  addItem,
  deleteItem,
  editItem
} from '../Redux/Action';
import styles from './Styles/ListScreenStyle';
import { Creators } from '../Redux/Reducers/AdvanceReducer';
import Modal from 'react-native-modal';

class ListScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddModalVisible: false,
      isUpdateModalVisible: false,
      text: '',
      selectedIndex: 0
    };
  }
    renderItem = ({ item, index }) => {
      const { deleteItem } = this.props;
      return(
        <View style={{ height:80,width:'90%',backgroundColor:'white',flexDirection:'row',alignItems:'center',alignSelf:'center' }}>
          <Text style={{ flex:1 }}>{item}</Text>
          <Button bordered danger onPress={()=> {
            this.props.deleteItem({ index:index });
          }}>
            <Text>Delete</Text>
          </Button>
          <Button bordered warning onPress={()=> {
            this.setState({ isUpdateModalVisible:true,isAddModalVisible:false,text:item,selectedIndex:index });
          }}>
            <Text>Update</Text>
          </Button>
        </View>
      );
    }
    renderModalView = (isAdd) => {
      return(
        <Modal
          testID={'modal'}
          isVisible={ isAdd ? this.state.isAddModalVisible : this.state.isUpdateModalVisible}
          backdropColor="#B4B3DB"
          backdropOpacity={0.8}
          animationIn="zoomInDown"
          animationOut="zoomOutUp"
          animationInTiming={600}
          animationOutTiming={600}
          backdropTransitionInTiming={600}
          backdropTransitionOutTiming={600}>
          <View style={{ flex:0.3,backgroundColor:'white',justifyContent:'center',alignItems:'center' }}>
            <TextInput
              style={{ height: 40,width:'90%' }}
              placeholder="Type here to translate!"
              value={this.state.text}
              onChangeText={(text) => this.setState({ text })}
            />
            <View style={{ width:'100%',height:1.5,backgroundColor:'black' }}/>
            <Button  info style={{ marginTop:25 }} onPress={()=> {
              if(this.state.text === ''){
                alert('name of toDo can not be empty');
                return;
              }
              isAdd ? this.props.addItem({ item:this.state.text }) : this.props.updateItem({ item:this.state.text,index:this.state.selectedIndex });
              isAdd ? this.setState({ isAddModalVisible:false }) : this.setState({ isUpdateModalVisible:false });
            }}>
              <Text>{isAdd ? 'Add' : 'Update'}</Text>
            </Button>
          </View>
        </Modal>
      );
    }
    render() {
      const { list } = this.props;
      return (
        <SafeAreaView
          style={styles.container}
          forceInset={{ bottom: 'always', top: 'never' }}>
          <Container style={styles.container}>
            <Header
              onLeftPress={() => {
                this.props.navigation.goBack();
              }}
              onRightPress={() => this.setState({ isAddModalVisible: true })}
            />
            <View style={{ height:80,width:'100%',backgroundColor:'white',justifyContent:'center',alignItems:'center' }}>
              <Text>Todo list</Text>
              <Button bordered info onPress={()=> {
                this.setState({ text:'',isAddModalVisible:true });
              }}>
                <Text>Add</Text>
              </Button>
            </View>
            <FlatList
              style={styles.list}
              data={list}
              renderItem={this.renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
            {this.renderModalView(true)}
            {this.renderModalView(false)}
          </Container>
        </SafeAreaView>
      );
    }
}
const mapStateToProps = (state, ownProps) => {
  let reduxMethod = ownProps.navigation.getParam('type', '');
  if(reduxMethod === 'basic') {
    return { list: state.BasicReducer.list };
  } else if (reduxMethod === 'advance') {
    return { list: state.AdvanceReducer.list };
  } 
};
const mapDispatchToProps = (dispatch, ownProps) => {
  let reduxMethod = ownProps.navigation.getParam('type', '');
  return {
    addItem: ({ item }) => {
      if (reduxMethod === 'basic') {
        dispatch(addItem({ item }));
      } else if (reduxMethod === 'advance') {
        dispatch(Creators.addItem({ item }));
      }
    },
    updateItem: ({ index, item }) => {
      if (reduxMethod === 'basic') {
        dispatch(editItem({ index, item }));
      } else {
        dispatch(Creators.editItem({ index,item }));
      }
    },
    deleteItem: ({ index }) => {
      if (reduxMethod === 'basic') {
        dispatch(deleteItem({ index }));
      } else {
        dispatch(Creators.deleteItem({ index }));
      }
    }
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ListScreen);
