import React from 'react';
import { View, Text,StyleSheet } from 'react-native';
import { Button } from 'native-base';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.body}>
          <Button bordered large info style={{ width:250,justifyContent:'center' }} onPress={()=>{
            this.props.navigation.navigate('List', {
              type: 'basic'
            });
          }}>
            <Text>Normal way</Text>
          </Button>
          <Button bordered large info style={{ width:250,justifyContent:'center' }} onPress={()=>{
            this.props.navigation.navigate('List', {
              type: 'advance'
            });
          }}>
            <Text>ReduxSauce way</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex:1,
    alignItems:'center',
    justifyContent:'center'
  },
  body: {
    flex:0.5,
    alignItems:'center',
    justifyContent:'space-around'
  }
});

export default HomeScreen;
