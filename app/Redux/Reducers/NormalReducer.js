import { DELETE_ITEMS,ADD_ITEMS, EDIT_ITEMS } from '../Types';

const initialState = {
  list: ['abxc','dfds','fsdfds','fdsfs']
};

const ListReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
  case DELETE_ITEMS:{
    alert('dworkdfgklg');
    let temp = [...state.list];
    if (payload.index > -1) {
      temp.splice(payload.index, 1);
    }
    return{
      ...state,
      list:temp
    };
  }
  case EDIT_ITEMS:{
    let temp = [...state.list];
    if (payload.index > -1) {
      temp[payload.index] = payload.item;
    }
    return{
      ...state,
      list:temp
    };
  }
  case ADD_ITEMS:{
    let temp = [...state.list];
    if (payload.item) {
      temp.push(payload.item);
    }
    return{
      ...state,
      list:temp
    };
  }

  default:
    return state;
  }
};

export default ListReducer;
