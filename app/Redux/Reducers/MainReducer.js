import { combineReducers } from 'redux';
import BasicReducer from './NormalReducer';
import AdvanceReducer from './AdvanceReducer';

const rootReducer = combineReducers({
  BasicReducer: BasicReducer,
  AdvanceReducer: AdvanceReducer
});
export default rootReducer;
