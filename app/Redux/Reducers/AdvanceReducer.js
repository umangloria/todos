import { createActions, createReducer } from 'reduxsauce';

const initialState = {
  list: ['fdf','unknds','hjklbv','ggh']
};

export const { Types, Creators } = createActions({
  addItem: ['payload'],
  editItem: ['payload'],
  deleteItem: ['payload']
});

const addItem = (state = initialState, action) => {
  const { payload } = action;
  let temp = [...state.list];
  if (payload.item) {
    temp.push(payload.item);
  }
  return{
    ...state,
    list:temp
  };
};

const editItem = (state = initialState, action) => {
  const { payload } = action;
  let temp = [...state.list];
  if (payload.index > -1) {
    temp[payload.index] = payload.item;
  }
  return{
    ...state,
    list:temp
  };
};

const deleteItem = (state = initialState, action) => {
  alert('Advanced');
  const { payload } = action;
  let temp = [...state.list];
  if (payload.index > -1) {
    temp.splice(payload.index, 1);
  }
  return{
    ...state,
    list:temp
  };
};

const HANDLERS = {
  [Types.ADD_ITEM]: addItem,
  [Types.EDIT_ITEM]: editItem,
  [Types.DELETE_ITEM]: deleteItem
};

const reducer = createReducer(initialState, HANDLERS);

export default reducer;
