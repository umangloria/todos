import { DELETE_ITEMS, EDIT_ITEMS, ADD_ITEMS } from './Types';

export const deleteItem = ({ index }) => {
  return {
    type: DELETE_ITEMS,
    payload: {
      index: index
    }
  };
};

export const editItem = ({ index, item }) => {
  return {
    type: EDIT_ITEMS,
    payload: {
      index: index,
      item: item
    }
  };
};

export const addItem = ({ item }) => {
  return {
    type: ADD_ITEMS,
    payload: {
      item: item
    }
  };
};
