/* eslint-disable no-undef */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import MainContainer from './app/MainContainer';
import { createStore } from 'redux';
import reducer from './app/Redux/Reducers/MainReducer';
import { Provider } from 'react-redux';
const store = createStore(reducer);


class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <MainContainer />
      </Provider>
    );
  }
}

export default App;
